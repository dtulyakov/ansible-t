#!/bin/sh
apt update -qq
apt install python3-pip python3-apt
pip3 install pip --upgrade
pip install ansible
ansible-playbook playbooks/init.yml -l localhost --ask-become -D
